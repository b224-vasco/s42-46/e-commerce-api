const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "E-mail is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default:false
	},
	orderedProduct: [
		{
			products: [
				{
					productId: {
						type: String,
						required: [true, "ID required"]
					},
					productName: {
						type: String,
						default: new String()
						// required: [true, "Product Name is required"]
					},

					quantity: {
						type: Number,
						default: new Number()
					}
				}
			],

			totalAmount: {
				type: Number,
				default: new Number()
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			},
		}

	]
});

module.exports = mongoose.model("User", userSchema);