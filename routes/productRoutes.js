const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require("../auth");


// Routes
// Route for creating a product
router.post("/addProduct", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData)
	console.log("wassup")
	console.log(req.headers.authorization);
	if(userData.isAdmin == true){
		productController.addProduct(req.body).then(resultFromController =>
			res.send(resultFromController))
	} else{
		res.send(false);
	}
});


// Route for retrieving all the active products
router.get("/getall", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	productController.getAllActiveProducts(userData).then(resultFromController => res.send(resultFromController))

});

// Route for retrieving all products -ADMIN
router.get("/getallprod", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == true){
		productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false);
	}

});


// Route for retrieving a specific product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId)

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});


// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin == true){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false)
	}
});

router.put("/archive/:productId", auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true){
		productController.archiveProduct(req.params, req.body).then((resultFromController) => res.send(resultFromController))
	}else{
		res.send(false)
	}
})

router.put("/activate/:productId", auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true){
		productController.activateProduct(req.params, req.body).then((resultFromController) => res.send(resultFromController))
	}else{
		res.send(false)
	}
})


module.exports = router;