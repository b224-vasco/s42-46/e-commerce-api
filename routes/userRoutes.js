const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");


// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for user log in
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for enrolling an authenticated user
router.post("/checkout", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId: userData.id,
		productId: req.body.productId,
		// orderId: req.body.orderId
	}
	
	console.log(userData)
	if(userData.isAdmin == false){
		userController.checkout(data).then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false)
	}

})

// Retrieve user details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData)
	console.log(req.headers.authorization);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))
});

// Setting user as an admin

router.put("/setAdmin", auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true){
		userController.setUserAdmin({userId: req.body.id}).then((resultFromController) => res.send(resultFromController))
	}else{
		res.send(false)
	}
})


router.get("/getOrders", auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true){
		userController.getAllOrders().then((resultFromController) => res.send(resultFromController))
	}else{
		res.send(false)
	}
})


module.exports = router;