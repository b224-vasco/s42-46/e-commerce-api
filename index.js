const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");


const app = express();
const port = process.env.PORT || 4000;

// Mongoose Connection setup
mongoose.connect("mongodb+srv://Tristan_224:admin123@batch-224-vasco.lmcsvxb.mongodb.net/ecommerce-API?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)

let db = mongoose.connection;

db.on("error", () => console.error("Connection error"));
db.once("open", () => console.log("Connected to MongoDB"));



// Middlewares
// Allows our frontend app to access our backend app.
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Main URI
app.use("/users", userRoutes);
app.use("/products", productRoutes);




app.listen(port, () => {console.log(`API is now running at port: ${port}`)})