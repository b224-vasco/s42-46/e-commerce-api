const Product = require("../models/Product");

// Creating a new product
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newProduct.save().then((product, error) => {
		if(error){
			return false // "Course creation failed"
		} else {
			return true // "Course creation succeeded"
		}
	})
};

// Getting all active products
module.exports.getAllActiveProducts = (data) => {

	return Product.find({isActive: true}).then(result => {
		return result
	})

}

// Getting all products - ADMIN
module.exports.getAllProducts = (data) => {

	if(data.isAdmin){
		return Product.find().then(result => {
			return result
		})
	} else {
		return false // "You are not an Admin"
	}
	
}


// Retrieving a Specific Product
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result
	})
};

// update a Specific Course
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((updatedProduct, error) => {

		if(error){
			return false
		} else{
			return true
		}
	}) 
}

// Archive Product
module.exports.archiveProduct = (reqParams, reqBody) => {
	let archivedProduct = {
		isActive: reqBody.isActive
	}

	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((archivedProduct, error) => {

		if(error){
			return false
		} else{
			return true
		}
	}) 
};

// Activate Product
module.exports.activateProduct = (reqParams, reqBody) => {
	let activatedProduct = {
		isActive: reqBody.isActive
	}

	return Product.findByIdAndUpdate(reqParams.productId, activatedProduct).then((archivedProduct, error) => {

		if(error){
			return false
		} else{
			return true
		}
	}) 
};

