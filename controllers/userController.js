const User = require('../models/User');
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		// User registration failed
		if(error){
			return false
		// User registration successful
		}else{
			return true
		}
	})
};

// User Log In
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return{access: auth.createAccessToken(result)}
			}else{
				return false
			}
		}
	})
};

// Creates an order
module.exports.checkout = async(data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {
		console.log(user);
		if(user.orderedProduct[0]){
			user.orderedProduct[0].products.push({productId: data.productId});
		} else {
			user.orderedProduct[0] = {products: [{productId: data.productId}]}
		}

		return user.save().then((user, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(
		product => {

			product.userOrder.push({userId: data.userId, orderId:data.orderId});

			return product.save().then((product, error) => {
				if(error){
					return false
				} else {
					return true
				}
			})	
		});

	if(isUserUpdated && isProductUpdated) {
		return true
	} else {
		return false
	};
};

// Retrieving user details
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if(result == null){
			return false
		}else{
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};

// Setting User as Admin
module.exports.setUserAdmin = (reqParams, reqBody) => {
	// let setAdmin = {
	// 	isAdmin: reqBody.isAdmin,
	// }

	return User.findByIdAndUpdate(reqParams.userId, {isAdmin: true}).then((user, error) => {
		if(error){
			return false
		} else{
			return true
		}
	}) 
}

// Getting all orders - ADMIN
module.exports.getAllOrders = (data) => {

	return User.find({}).then(users => {
		let orders = [];
		users.forEach((user) =>{
			orders.push(user.orderedProduct)
		})
		console.log(orders)
		return orders
	})
	
}
